Scala projects

For practicing Scala

Project 1

Purpose: Calculating some metrics over stock price data
Data source: https://www.quandl.com/api/v3/datasets/WIKI/<symbol>.json , download it to a file
Example: Apple Inc: https://www.quandl.com/api/v3/datasets/WIKI/AAPL.json

Format:

```
{
    "dataset": {
        "id": 9775409,
        "dataset_code": "AAPL",
        
        "column_names": [
              "Date",
              "Open",
              "High",
              "Low",
              "Close",
              "Volume",
              "Ex-Dividend",
              "Split Ratio",
              "Adj. Open",
              "Adj. High",
              "Adj. Low",
              "Adj. Close",
              "Adj. Volume"
        ],

        "data": [
          [ // follow the same order as the column_names above
            "2018-03-27",
            173.68,
            175.15,
            166.92,
            168.34,
            38962839,
            0,
            1,
            173.68,
            175.15,
            166.92,
            168.34,
            38962839
          ],
          // ........
        ]
    }
}
```

Program input:
* path to the Json file
* start date
* end date

Program should read the Json file, parse it, then calculate and output:

* min/max/avg of Open Price for the dates within <start date> and <end date>
    * you should not assume that Open price is the 2nd value in the data array. Instead you should parse the column_names field to know the exact location
* list of Simple Moving Average Price
    * Simple Moving Average for a date is calculated by summing the past 50 day Closing prices and dividing by 50. Ignore the first 50 days in the series since we don’t have enough 50 data points
* list of Linearly Weighted Moving Average Prices
    * LWMA is a type of moving average that assigns a higher weighting to recent price data than does the common simple moving average. This average is calculated by taking each of the closing prices over a given time period and multiplying them by its certain position in the data series. Once the position of the time periods have been accounted for they are summed together and divided by the sum of the number of time periods. So, today's closing price is multiplied by 15, yesterday's by 14, and so on until day 1 in the period's range is reached. These results are then added together and divided by the sum of the multipliers (15+14+13+...+3+2+1= 120). Again, ignore the first 15 days


Non-functional requirements:

* should avoid var as much as possible, use val instead
* should make use of the provided functions in standard library like fold, map ...
* should have unit test
* consider edge cases
* provide the most efficient solution possible
* ???

