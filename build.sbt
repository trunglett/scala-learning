name := "scala-learning"

version := "0.1"

scalaVersion := "2.13.1"

libraryDependencies += "com.softwaremill.sttp.client" %% "core" % "2.0.0-RC6"
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.8.1"

libraryDependencies += "org.scalatest" % "scalatest_2.13" % "3.1.0" % "test"