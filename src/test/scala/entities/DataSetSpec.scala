package entities

import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.must.Matchers

class DataSetSpec extends AnyFreeSpec with Matchers {
  val id = 9775409
  val code = "AAPL"
  val columns: Seq[String] = Seq(
    "Date",
    "Open",
    "High",
    "Low",
    "Close",
    "Volume",
    "Ex-Dividend",
    "Split Ratio",
    "Adj. Open",
    "Adj. High",
    "Adj. Low",
    "Adj. Close",
    "Adj. Volume"
  )
  val data: Seq[Seq[DataValue]] = Seq(
    Seq(
      DataValueString("2018-03-27"),
      DataValueNumber(173.68),
      DataValueNumber(175.15),
      DataValueNumber(166.92),
      DataValueNumber(168.34),
      DataValueNumber(38962839.0),
      DataValueNumber(0.0),
      DataValueNumber(1.0),
      DataValueNumber(173.68),
      DataValueNumber(175.15),
      DataValueNumber(166.92),
      DataValueNumber(168.34),
      DataValueNumber(38962839.0)
    ),
    Seq(
      DataValueString("2018-03-26"),
      DataValueNumber(168.07),
      DataValueNumber(173.1),
      DataValueNumber(166.44),
      DataValueNumber(172.77),
      DataValueNumber(36272617.0),
      DataValueNumber(0.0),
      DataValueNumber(1.0),
      DataValueNumber(168.07),
      DataValueNumber(173.1),
      DataValueNumber(166.44),
      DataValueNumber(172.77),
      DataValueNumber(36272617.0)
    )
  )

  "initializer" - {
    "with not empty columns" - {
      "and not empty data" - {
        val dataset = DataSet(id, code, columns, data)

        "must create correct val dataRows" in {
          dataset.dataRows mustEqual Array(
            DataRow(
              "2018-03-27",
              173.68,
              168.34
            ),
            DataRow(
              "2018-03-26",
              168.07,
              172.77
            )
          )
        }
      }

      "and empty data" - {
        val dataset = DataSet(id, code, columns, Seq())
        "must create correct val dataRows" in {
          dataset.dataRows mustEqual Seq()
        }
      }
    }
  }
}
