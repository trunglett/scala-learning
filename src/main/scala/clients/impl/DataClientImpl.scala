package clients.impl

import clients.DataClient
import sttp.client._


object DataClientImpl extends DataClient {
  override def get(url: String): Either[Throwable, String] = {
    val request = basicRequest.get(uri"$url")
    implicit val backend: SttpBackend[Identity, Nothing, NothingT] = HttpURLConnectionBackend()
    val response = request.send()

    if (response.body.isLeft) {
      Left(new Throwable(response.body.left.getOrElse("API error")))
    } else {
      Right(response.body.getOrElse("{}"))
    }
  }
}
