package clients

trait DataClient {
  def get(url: String): Either[Throwable, String]
}
