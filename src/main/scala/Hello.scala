
import clients.impl.DataClientImpl
import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.JsonMappingException
import entities.DataSet
import play.api.libs.json.{JsResultException, Json, Reads}
import processors.DataProcessor
import utils.FunctionalJson._

import scala.io.StdIn._

object Hello extends App {
  println("URL:")
  val url = readLine()

  println("Start:")
  val startDate = readLine()

  println("End:")
  val endDate = readLine()

  implicit val datasetRead: Reads[DataSet] = DataSet.dateSetReads

  DataClientImpl.get(url)
    .map(parseJson)
    .joinRight
    .map(_ \ "dataset")
    .map(jsonToObject[DataSet])
    .joinRight
    .map(DataProcessor.execute(_, startDate, endDate))
    .fold(
      {
        case e: JsonParseException => println(s"String to Json error: [${e.getMessage}")
        case e: JsonMappingException => println(s"String to Json error: [${e.getMessage}")
        case e: JsResultException => println(s"Json to DataSet error:  [${e.getMessage}]")
        case e: Throwable => println(s"Error: Throwable [${e.getMessage}")
      },
      result => println(Json.prettyPrint(Json.toJson(result)))
    )
}