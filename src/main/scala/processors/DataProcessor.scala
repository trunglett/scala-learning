package processors

import entities.{DataSet, Result}

object DataProcessor {
  def execute(dataSet: DataSet, startDate: String, endDate: String): Result = {
    // get open prices in selected range
    val openPrices = dataSet.dataRows.filter(row => {
      row.date >= startDate && row.date <= endDate
    }).map(_.open)

    // get close prices which not exceeded end date
    val closePrices = dataSet.dataRows.dropWhile(_.date > endDate).map(_.close)
    // get close prices in selected range + 50 buffer rows to calculate sm average
    val closeSMPrices = closePrices.take(openPrices.length + 50)
    // get close prices in selected range + 15 buffer rows to calculate lwma average
    val closeLWMAPrices = closePrices.take(openPrices.length + 15)

    Result(
      openPrices.min,
      openPrices.max,
      findAvg(openPrices),
      findSMAvg(closeSMPrices, 50),
      findLWMAvg(closeLWMAPrices, 15)
    )
  }

  def findAvg(prices: Seq[BigDecimal]): BigDecimal = {
    prices.fold(BigDecimal(0)) { (avg, price) => avg + price / prices.length }
  }

  def findSMAvg(prices: Seq[BigDecimal], smRange: Int): Seq[BigDecimal] = {
    if (prices.length < smRange + 1) {
      return Seq()
    }

    var lastSum = prices.take(smRange).fold(BigDecimal(0)) { (sum, price) => sum + price }
    var currentIndex = -1
    prices.take(prices.length - smRange).map(price => {
      currentIndex += 1
      lastSum = lastSum - price + prices(currentIndex + smRange)
      lastSum / smRange
    })
  }

  def findLWMAvg(prices: Seq[BigDecimal], lwmRange: Int): Seq[BigDecimal] = {
    if (prices.length < lwmRange + 1) {
      return Seq(0)
    }
    val multiply = (1 to lwmRange).sum
    prices.sliding(lwmRange)
      .filter(_.length == lwmRange)
      .map(_.zipWithIndex.foldLeft(BigDecimal(0)) {
        (avg, value) => avg + value._1 * (lwmRange - value._2) / multiply
      })
      .toSeq
  }
}
