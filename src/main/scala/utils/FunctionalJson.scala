package utils

import play.api.libs.json._

import scala.util.Try

object FunctionalJson {
  def parseJson(text: String): Either[Throwable, JsValue] = {
    Try(Json.parse(text)).toEither
  }

  def jsonToObject[T](json: JsReadable)(implicit reads: Reads[T]): Either[Throwable, T] = {
    Try(json.as[T]).toEither
  }
}
