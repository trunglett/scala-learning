package entities

import play.api.libs.json.{JsNumber, Json, OFormat, Writes}

import scala.math.BigDecimal.RoundingMode

case class Result(min: BigDecimal, max: BigDecimal, avg: BigDecimal, smAvg: Seq[BigDecimal], lwmAvg: Seq[BigDecimal])

object Result {
  implicit def resultFormat: OFormat[Result] = Json.format[Result]
  implicit val bigDecimalWrites: Writes[BigDecimal] = (o: BigDecimal) => {
    JsNumber(o.setScale(2, RoundingMode.HALF_UP))
  }
}