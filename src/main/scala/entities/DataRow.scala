package entities

import play.api.libs.json.{Json, OFormat}

case class DataRow(date: String, open: BigDecimal, close: BigDecimal)

object DataRow {
  implicit def dataRowFormat: OFormat[DataRow] = Json.format[DataRow]
}