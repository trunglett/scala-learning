package entities

import play.api.libs.json.{JsNumber, JsString, JsSuccess, Reads}

sealed trait DataValue
case class DataValueString(value: String) extends DataValue
case class DataValueNumber(value: BigDecimal) extends DataValue

object DataValue {
  implicit val dataValueReads: Reads[DataValue] = {
    case JsString(s) => JsSuccess(DataValueString(s))
    case JsNumber(s) => JsSuccess(DataValueNumber(s))
    case _ => throw new Exception("Invalid json value") // TODO Add better error handling
  }
}