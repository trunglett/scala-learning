package entities

import play.api.libs.json.{JsPath, Json, OFormat, Reads}
import play.api.libs.functional.syntax._

case class DataSet(id: Int, code: String, dataRows: Seq[DataRow])

object DataSet {
  implicit def datasetFormat: OFormat[DataSet] = Json.format[DataSet]

  // alternative[1] constructor
  def apply(id: Int, code: String, columns: Seq[String], data: Seq[Seq[DataValue]]): DataSet = {
    val columnMap: Map[String, Int] = columns.zipWithIndex.map({ case (name, index) => (name, index) }).toMap
    val dataRows: Seq[DataRow] = data.map(row => {
      DataRow(
        row(columnMap("Date")).asInstanceOf[DataValueString].value,
        row(columnMap("Open")).asInstanceOf[DataValueNumber].value,
        row(columnMap("Close")).asInstanceOf[DataValueNumber].value
      )
    })
    new DataSet(id, code, dataRows)
  }

  // alternative[1] read json
  def dateSetReads: Reads[DataSet] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "dataset_code").read[String] and
      (JsPath \ "column_names").read[Seq[String]] and
      (JsPath \ "data").read[Seq[Seq[DataValue]]]
    ) (apply(_, _, _, _))
}